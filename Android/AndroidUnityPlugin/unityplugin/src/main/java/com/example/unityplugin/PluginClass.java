package com.example.unityplugin;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.os.Build;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.Display;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.unity3d.player.UnityPlayer;
import com.unity3d.player.UnityPlayerActivity;

import java.lang.reflect.Field;

import static android.opengl.GLES20.glBindTexture;
import static android.opengl.GLES20.glTexParameteri;
import static com.unity3d.player.UnityPlayer.UnitySendMessage;

/**
 * Created by Appearition on 4/12/2017.
 */

public class PluginClass {
    public static UnityPlayerActivity mContext;
    public static UnityPlayer mUnityPlayer;
    private static int mTexturePointer = 0;
    private static float xPosition;
    private static float yPosition;
    private static WebView myWebView;
    private static FrameLayout frameLayout;
    private static GLSurfaceView mGLView;


    public static FrameLayout AddFrameLayout(final Activity activity, final int x, final int y, float xWidth, float yHeight) {
        frameLayout = new FrameLayout(activity);
        frameLayout.setLayoutParams(new FrameLayout.LayoutParams(
                FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT));
        // mGLView = new MyGLSurfaceView(activity);

        activity.addContentView(frameLayout, new ViewGroup.LayoutParams
                (ViewGroup.LayoutParams.MATCH_PARENT,
                        ViewGroup.LayoutParams.MATCH_PARENT));
        // MoveTo(frameLayout,  (int) ( x - (3*x / 4)+xPosition*(5*x/2)),  (y - (y /2 )-yPosition*(y/20)));
//        if(yPosition < 0 ){
//            MoveTo(frameLayout,  (int) ( x - (3*x / 4)+xPosition*x/10),  (y - (y /2 )+yPosition*9*y/20));
//
//        }else{
        //  MoveTo(frameLayout, (int) (x - (3 * x / 4) + xPosition * (xPosition / 10)), (int) (y - (y / 2) - yPosition * (yPosition / 10)));
        // MoveTo(frameLayout, (int) (x - (3 * x / 4) + xPosition * (x / 10)), (int) (y - (y / 2) - yPosition * (y / 10)));
        //MoveTo(frameLayout, (int) (x - (3 * x / 4 * xWidth) + xPosition * (x / 10)), (int) (y - (y * yHeight / 2) - yPosition * (y / 10)));

        return frameLayout;
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static WebView AddWebView(final UnityPlayerActivity context, final ViewGroup parent,
                                     String url, float Width, float yHeight, boolean isUpdate) {
        myWebView = new WebView(context);
        myWebView.setVisibility(View.GONE);
        // myWebView.setLayoutParams(new ViewGroup.LayoutParams((int) (Width * (3 * getScreenWidth() / 4)), (int) (yHeight * (9 * getScreenHeight() / 20))));
        myWebView.setLayoutParams(new ViewGroup.LayoutParams((int) (Width * (3 * getScreenWidth() / 4) + getScreenWidth() / 10), (int) (yHeight * (11 * getScreenHeight() / 20))));
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setUserAgentString("Android Mozilla/5.0 AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30");
        webSettings.setMediaPlaybackRequiresUserGesture(false);
        myWebView.setHorizontalScrollBarEnabled(true);
        myWebView.setVerticalScrollBarEnabled(true);
        myWebView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                UnitySendMessage("WebviewPlane", "sendEventStatus", "onPageStarted");
            }

            @Override
            public void onPageFinished(final WebView view, String url) {
                super.onPageFinished(view, url);
                UnitySendMessage("WebviewPlane", "sendEventStatus", "onPageFinished");
                myWebView = view;
                // createBitmapFromView(view);
                Message message = mHandler.obtainMessage();
                mHandler.sendMessage(message);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                UnitySendMessage("WebviewPlane", "sendEventStatus", error.toString());
            }
        });
        myWebView.setWebChromeClient(new WebChromeClient());
        if (url.contains("vimeo")) {
            url = url.replace("https://vimeo.com/", "http://player.vimeo.com/video/");
            url = url + "?player_id=player&title=0&byline=0&portrait=0&autoplay=1&api=1";
        }
        if (!isUpdate)
            myWebView.loadUrl(url);
        //adding view to layout
        parent.addView(myWebView);
        //setGameObjectView(context);

        return myWebView;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static WebView AddWeb(UnityPlayerActivity activity, String url, float x, float y, float xWidth, float yHeight, boolean isUpdate) {
        mContext = activity;
        xPosition = x;
        yPosition = y;
        int xCenter = getScreenWidth() / 2;
        int yCenter = getScreenHeight() / 2;

        frameLayout = AddFrameLayout(activity, xCenter, yCenter, xWidth, yHeight);
        myWebView = AddWebView(activity, frameLayout, url, xWidth, yHeight, isUpdate);
        MoveTo(activity, myWebView, (int) (xCenter - (3 * xCenter / 4 * xWidth) + xPosition * (xCenter / 10)), (int) (yCenter - (yCenter * yHeight / 2) - yPosition * (yCenter / 10)));
        return myWebView;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static WebView AddWebView(final UnityPlayerActivity activity, final String url, final float x, final float y, final int texturePtr, float Width, float Height, boolean isUpdate) {

        mTexturePointer = texturePtr;
        return AddWeb(activity, url, x, y, Width, Height, isUpdate);

    }

    public static void MoveTo(UnityPlayerActivity activity, final View view, final int newX, final int newY) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                view.setX(newX);
                view.setY(newY);
                myWebView.setVisibility(View.VISIBLE);
            }
        });

    }


    public static void callActivity(final UnityPlayerActivity activity, final String url) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(activity, Test.class);
                // intent.putExtra("KEY_ARGUEMENT_EDUCATION_VIDEO_URL",url);
                activity.startActivity(intent);
            }
        });
    }

    public static void textureRenderObject(final int texturePtr, final UnityPlayerActivity activity) {
        mTexturePointer = texturePtr;
        if (texturePtr != 0) {
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;
            options.inJustDecodeBounds = false;
            Bitmap bitmap = BitmapFactory.decodeResource(activity.getResources(),
                    R.drawable.quotes);
            glBindTexture(GLES20.GL_TEXTURE_2D, texturePtr);
            glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
            // GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, GLES20.GL_RGBA, bitmap, GLES20.GL_UNSIGNED_BYTE, 0);

            Log.i("VideoHandler", "Recieved ID: " + texturePtr);

            bitmap.recycle();
        }
    }

 /*   public static void  createBitmapFromView(View view) {
        //Pre-measure the view so that height and width don't remain null.
        view.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

        //Assign a size and position to the view and all of its descendants
        view.layout(0, 0, view.getMeasuredWidth(), view.getMeasuredHeight());

        //Create the bitmap
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(),
                view.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        //Create a canvas with the specified bitmap to draw into
        Canvas c = new Canvas(bitmap);

        //Render this view (and all of its children) to the given Canvas
        view.draw(c);
        mBitmap = bitmap;
    }*/

    private static Handler mHandler = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {

            // final BitmapFactory.Options options = new BitmapFactory.Options();
            //    options.inScaled = false;
            //   options.inJustDecodeBounds = false;
           /* glBindTexture(GLES20.GL_TEXTURE_2D, mTexturePointer);
            glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
            glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);

            GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, mBitmap, 0);
            Log.i("VideoHandler", "Recieved ID: " + mTexturePointer);
            mBitmap.recycle();*/
            //   createTexture(mBitmap);
            return false;
        }
    });

    public static int createTexture(Bitmap bitmap) {
        final int[] textureHandle = new int[1];
        GLES20.glGenTextures(1, textureHandle, 0);
        glBindTexture(GLES20.GL_TEXTURE_2D, mTexturePointer);
        glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_NEAREST);
        glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_NEAREST);
        updateTexture(textureHandle[0], bitmap);
        return textureHandle[0];
    }

    public static void updateTexture(int textureName, Bitmap bitmap) {
        glBindTexture(GLES20.GL_TEXTURE_2D, textureName);
        // Load the bitmap into the bound texture.
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);
    }

    public static int getScreenWidth() {
        Display mdisp = mContext.getWindowManager().getDefaultDisplay();
        Point mdispSize = new Point();
        mdisp.getSize(mdispSize);
        return mdispSize.x;

        //  return Resources.getSystem().getDisplayMetrics().widthPixels;
    }

    public static int getScreenHeight() {
        Display mdisp = mContext.getWindowManager().getDefaultDisplay();
        Point mdispSize = new Point();
        mdisp.getSize(mdispSize);
        return mdispSize.y;
        // return Resources.getSystem().getDisplayMetrics().heightPixels;
    }

    public static float getPixeltoDP(Activity activity, float pix) {
        float density = activity.getResources().getDisplayMetrics().density;
        float dp = pix / density;
        return dp;
    }


    public static void callSurfaceArea(final UnityPlayerActivity context) {
        UnityPlayer view = (UnityPlayer) mUnityPlayer.getView();
        Field f = null;
        try {
            f = view.getClass().getDeclaredField("i");
            f.setAccessible(true);
            SurfaceView sf = (SurfaceView) f.get(view);
            mGLView = new MyGLSurfaceView(context);
            sf = mGLView;
            SurfaceHolder sfhTrackHolder = sf.getHolder();
            // sfhTrackHolder.addCallback(myHolderCallback);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static void callUpdateWebView(final UnityPlayerActivity activity, final float x, final float y, final float xWidth, final float yHeight) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                xPosition = x;
                yPosition = y;
                int xCenter = getScreenWidth() / 2;
                int yCenter = getScreenHeight() / 2;
                MoveTo(activity, myWebView, (int) (xCenter - (3 * xCenter / 4 * xWidth) + xPosition * (xCenter / 10)), (int) (yCenter - (yCenter * yHeight / 2) - yPosition * (yCenter / 10)));
                myWebView.setVisibility(View.VISIBLE);
            }
        });
    }


}
