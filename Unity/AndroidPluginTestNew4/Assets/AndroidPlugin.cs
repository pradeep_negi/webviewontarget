﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AndroidPlugin : MonoBehaviour
{
	public string urlString;
	private  float XPosition;
	private float  YPosition;
	public Text text;
	public AndroidJavaObject webView;
	private GameObject mWebviewPlane;
	private float xPixelDistance;
	private float yPixelDistance,width,height, Uwidth,depth;
	private Vector3 upperLeftPoint;
	private static int x = 0, y = 0;
	// Use this for initialization
    private Vector3 lowerLeftPoint ;



	void Update(){
		if (Application.platform == RuntimePlatform.Android) {
			// check the webview is not null 
			if (webView != null) {
				// left and top  pass to the native call
				mWebviewPlane = GameObject.Find ("WebviewPlane");
				width = mWebviewPlane.GetComponent<Renderer> ().bounds.size.x;
				height = mWebviewPlane.GetComponent<Renderer> ().bounds.size.y;
				XPosition = mWebviewPlane.transform.position.x;
				YPosition = mWebviewPlane.transform.position.z;


				var max = mWebviewPlane.GetComponent<Renderer> ().bounds.max;
				var min = mWebviewPlane.GetComponent<Renderer> ().bounds.min;

				Debug.Log ("****************MAXXXXXXX is : "+max+" MINNNNNNNN is : "+min+"***********************");


				Debug.Log ("****************xPosition is : "+(int)XPosition+" yPosition is : "+(int)YPosition+"***********************");

				Debug.Log ("This is the width pixel point: " + (int)width);
				Debug.Log(" This is the height pixel point: " + (int)height);

				Debug.Log ("****************Height is : "+height+" Width is : "+width+"***********************");
				//ajc.CallStatic ("callActivity", GetAndroidActivity ());
				//ajc.CallStatic ("callActivity", GetAndroidActivity (),urlString);
				depth = mWebviewPlane.transform.lossyScale.z;
				Uwidth = mWebviewPlane.transform.lossyScale.x;
				float Uheight = mWebviewPlane.transform.lossyScale.y;
				Debug.Log ("This is the z lossyScale point: " + depth);
				Debug.Log ("This is the x lossyScale point: " + Uwidth);
				Debug.Log ("This is the y lossyScale point: " + Uheight);


				Vector3 lowerLeftPoint = Camera.main.WorldToScreenPoint( new Vector3( mWebviewPlane.transform.position.x - Uwidth/2, mWebviewPlane.transform.position.y - Uheight/2, mWebviewPlane.transform.position.z - depth/2 ) );
				Vector3 upperRightPoint = Camera.main.WorldToScreenPoint( new Vector3( mWebviewPlane.transform.position.x + Uwidth/2, mWebviewPlane.transform.position.y + Uheight/2, mWebviewPlane.transform.position.z - depth/2 ) );
				upperLeftPoint = Camera.main.WorldToScreenPoint( new Vector3( mWebviewPlane.transform.position.x - Uwidth/2, mWebviewPlane.transform.position.y + Uheight/2, mWebviewPlane.transform.position.z - depth/2 ) );
				Vector3 lowerRightPoint = Camera.main.WorldToScreenPoint( new Vector3( mWebviewPlane.transform.position.x + Uwidth/2, mWebviewPlane.transform.position.y - Uheight/2, mWebviewPlane.transform.position.z - depth/2 ) );
				xPixelDistance = Mathf.Abs( lowerLeftPoint.x - upperRightPoint.x );
				yPixelDistance = Mathf.Abs( lowerLeftPoint.y - upperRightPoint.y );

				Debug.Log( "This is the X pixel distance: " + (int)xPixelDistance + " This is the Y pixel distance: " + yPixelDistance );
				Debug.Log ("This is the lower left pixel point: " + lowerLeftPoint);
				Debug.Log(" This is the upper left point: " + upperLeftPoint );
				Debug.Log ("This is the lower right pixel point: " + lowerRightPoint);
				Debug.Log(" This is the upper right pixel point: " + upperRightPoint);
				AndroidJavaObject activity = GetAndroidActivity ();
				activity.Call("runOnUiThread", new AndroidJavaRunnable(runOnUiThread1));
				//var ajc = new AndroidJavaClass ("com.example.unityplugin.PluginClass");

				//ajc.CallStatic("callUpdateWebView",  GetAndroidActivity ());

					}
		}
	}
	void Start ()
	{
  
		//if (Application.platform == RuntimePlatform.Android) {
		mWebviewPlane = GameObject.Find ("WebviewPlane");
		 width = mWebviewPlane.GetComponent<Renderer> ().bounds.size.x;
		 height = mWebviewPlane.GetComponent<Renderer> ().bounds.size.y;
		var max = mWebviewPlane.GetComponent<Renderer> ().bounds.max;
		var min = mWebviewPlane.GetComponent<Renderer> ().bounds.min;

		Debug.Log ("****************MAXXXXXXX is : "+max+" MINNNNNNNN is : "+min+"***********************");


		XPosition = mWebviewPlane.transform.position.x;
		YPosition = mWebviewPlane.transform.position.z;
		Debug.Log ("****************xPosition is : "+(int)XPosition+" yPosition is : "+(int)YPosition+"***********************");

		Debug.Log ("This is the width pixel point: " + (int)width);
		Debug.Log(" This is the height pixel point: " + (int)height);

		Debug.Log ("****************Height is : "+height+" Width is : "+width+"***********************");
		//ajc.CallStatic ("callActivity", GetAndroidActivity ());
		//ajc.CallStatic ("callActivity", GetAndroidActivity (),urlString);
		 depth = mWebviewPlane.transform.lossyScale.z;
		 Uwidth = mWebviewPlane.transform.lossyScale.x;
		float Uheight = mWebviewPlane.transform.lossyScale.y;
		Debug.Log ("This is the z lossyScale point: " + depth);
		Debug.Log ("This is the x lossyScale point: " + Uwidth);
		Debug.Log ("This is the y lossyScale point: " + Uheight);


		Vector3 lowerLeftPoint = Camera.main.WorldToScreenPoint( new Vector3( mWebviewPlane.transform.position.x - Uwidth/2, mWebviewPlane.transform.position.y - Uheight/2, mWebviewPlane.transform.position.z - depth/2 ) );
		Vector3 upperRightPoint = Camera.main.WorldToScreenPoint( new Vector3( mWebviewPlane.transform.position.x + Uwidth/2, mWebviewPlane.transform.position.y + Uheight/2, mWebviewPlane.transform.position.z - depth/2 ) );
		upperLeftPoint = Camera.main.WorldToScreenPoint( new Vector3( mWebviewPlane.transform.position.x - Uwidth/2, mWebviewPlane.transform.position.y + Uheight/2, mWebviewPlane.transform.position.z - depth/2 ) );
		Vector3 lowerRightPoint = Camera.main.WorldToScreenPoint( new Vector3( mWebviewPlane.transform.position.x + Uwidth/2, mWebviewPlane.transform.position.y - Uheight/2, mWebviewPlane.transform.position.z - depth/2 ) );
        xPixelDistance = Mathf.Abs( lowerLeftPoint.x - upperRightPoint.x );
		yPixelDistance = Mathf.Abs( lowerLeftPoint.y - upperRightPoint.y );

		Debug.Log( "This is the X pixel distance: " + (int)xPixelDistance + " This is the Y pixel distance: " + yPixelDistance );
		Debug.Log ("This is the lower left pixel point: " + lowerLeftPoint);
		Debug.Log(" This is the upper left point: " + upperLeftPoint );
		Debug.Log ("This is the lower right pixel point: " + lowerRightPoint);
		Debug.Log(" This is the upper right pixel point: " + upperRightPoint);

			AndroidJavaObject activity = GetAndroidActivity ();
		activity.Call("runOnUiThread", new AndroidJavaRunnable(runOnUiThread));
			
		//}  	
	}


	void runOnUiThread() {
		var ajc = new AndroidJavaClass ("com.example.unityplugin.PluginClass");
//		var cTexture = new Texture2D (100, 100,TextureFormat.RGBA32,false);
//		cTexture.Apply();
//		GetComponent<Renderer>().material.mainTexture = cTexture;
		//webView = ajc.CallStatic<AndroidJavaObject>("AddWebView", GetAndroidActivity (), urlString, upperLeftPoint.x, upperLeftPoint.y, cTexture.GetNativeTexturePtr().ToInt32(),Uwidth,depth);
	
		webView = ajc.CallStatic<AndroidJavaObject>("AddWebView", GetAndroidActivity (), urlString, XPosition, YPosition, 0,Uwidth,depth,false);
		ajc.CallStatic("callSurfaceArea",GetAndroidActivity ());

		//ajc.CallStatic("textureRenderObject", cTexture.GetNativeTexturePtr().ToInt32(), GetAndroidActivity ());
	}

	void runOnUiThread1() {
		var ajc = new AndroidJavaClass ("com.example.unityplugin.PluginClass");
		var cTexture = new Texture2D (100, 100,TextureFormat.RGBA32,false);
		cTexture.Apply();
		GetComponent<Renderer>().material.mainTexture = cTexture;
		//webView = ajc.CallStatic<AndroidJavaObject>("AddWebView", GetAndroidActivity (), urlString, upperLeftPoint.x, upperLeftPoint.y, cTexture.GetNativeTexturePtr().ToInt32(),Uwidth,depth);

		webView = ajc.CallStatic<AndroidJavaObject>("AddWebView", GetAndroidActivity (), urlString, XPosition+0.5, YPosition+0.5, 0,Uwidth,depth,true);

	}

	public static AndroidJavaObject GetAndroidActivity ()
	{
		AndroidJavaClass jc = new AndroidJavaClass ("com.unity3d.player.UnityPlayer");
		AndroidJavaObject jo = jc.GetStatic<AndroidJavaObject> ("currentActivity");
		return jo;

	}

	void sendEventStatus (string eventStatus)
	{
		switch (eventStatus) {
		case "onPageStarted":
			{
				onPageStarted ();
			}
			break;
			
		case "onPageFinished":
			{
				onPageFinished ();
			}
			break;
			
		case "onReceivedError":
			{
				onReceivedError ();
			}
			break;
		default:
			break;
		}
	}

	public void onPageStarted ()
	{
		text.text = "onPageStarted Received";
	}

	public void onPageFinished ()
	{
			var ajc = new AndroidJavaClass ("com.example.unityplugin.PluginClass");
			ajc.CallStatic("callUpdateWebView",  GetAndroidActivity(),XPosition, YPosition, Uwidth,depth);

		}

	public void onReceivedError ()
	{
		text.text = "onReceivedError Received";
	}

		
}
