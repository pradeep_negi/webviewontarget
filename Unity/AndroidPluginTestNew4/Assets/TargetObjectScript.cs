﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetObjectScript : MonoBehaviour {

	public GameObject mTargetObject;
	public GameObject mWebviewPlane;
	public GameObject mChildPlane;

	// Use this for initialization
	void Start () {
		Vector3 bounds = mTargetObject.GetComponent<MeshRenderer>().bounds.size;
		mWebviewPlane.transform.localScale = new Vector3(bounds.x,bounds.z,bounds.z);
		//mWebviewPlane.transform.localScale = mTargetObject.transform.lossyScale;
		mWebviewPlane.transform.position = mTargetObject.transform.position;

		//mChildPlane.transform.localScale = new Vector3(bounds.x,bounds.z,bounds.z);
		//mWebviewPlane.transform.localScale = mTargetObject.transform.lossyScale;
		//mChildPlane.transform.position = mTargetObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		//	GameObject mWebviewPlane = GameObject.Find("Cube");

//		float depth = mWebviewPlane.transform.localScale.z;
//		float Uwidth = mWebviewPlane.transform.localScale.x;
//		Debug.Log("This is the depth pixel point: " + depth);
//		Debug.Log(" This is the Uwidth pixel point: " + Uwidth);
//
//
//
//		//	GameObject mTargetObject = GameObject.Find("TargetObject");
//		float targetWidth = mTargetObject.GetComponent<Renderer>().bounds.size.x;
//		float targetHeight = mTargetObject.GetComponent<Renderer>().bounds.size.z;
//		float targetDepth = mTargetObject.GetComponent<Renderer>().bounds.size.y;
//
//		Debug.Log("This is the targetWidth pixel point: " + targetWidth);
//		Debug.Log(" This is the targetHeight pixel point: " + targetHeight);
//		Debug.Log(" This is the targetDepth pixel point: " + targetDepth);
//
//		float mDepth = mTargetObject.transform.localScale.z;
//		float mUwidth = mTargetObject.transform.localScale.x;
//		Debug.Log("This is the mDepth pixel point: " + mDepth);
//		Debug.Log(" This is the mUwidth pixel point: " + mUwidth);
//		float width = mWebviewPlane.GetComponent<Renderer>().bounds.size.x;
//		float height = mWebviewPlane.GetComponent<Renderer>().bounds.size.z;
//
//		mWebviewPlane.transform.localScale = new Vector3(targetWidth,targetHeight,1f);
//		mChildPlane.transform.localScale = new Vector3(width,height,1f);

	}
}
